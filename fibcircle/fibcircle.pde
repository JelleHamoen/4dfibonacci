//Sources:
// https://www.maa.org/sites/default/files/images/upload_library/23/picado/seashells/introdeng.html 
// https://math.stackexchange.com/questions/2258581/is-there-a-3d-fibonacci-sequence 
// Written by Jelle Hamoen

int xCords[]= new int[15];
int yCords[]= new int[15];
int fibseq[] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584};
String gradient[] = 
  {"FD6F0899", "E8671A99", "D35F2C99", "BF573E99", "AA505199", "95486399", "81407599", "6C388799", "57319A99", "4329AC99", "2E21BE99", "1A1AD199", "1A1AD199", "1A1AD199"} ;

void setup() {
  size(1024, 1024, P3D);
  frameRate(1);
  gradient = reverse(gradient);
}

void draw() {
  translate(512, 512, 0);
  rotateX(radians(45)); 
  //rotateZ(radians(45)); // uncomment to change perspective
  background(255);
  strokeWeight(1);
  int direction = 1;
  int x = 512;
  int y = 512;
  noFill();
  smooth();
  beginShape();
  for (int i = 0; i< 15; i++) {
    switch(direction) {
    case 1:
      x += fibseq[i];
      y += fibseq[i];

      vertex(x, y, 0);
      direction++;
      break;
    case 2:
      x -= fibseq[i];
      y += fibseq[i];

      vertex(x, y, 0);
      direction++;
      break;
    case 3:
      x -= fibseq[i];
      y -= fibseq[i];

      vertex(x, y, 0);
      direction++;
      break;
    case 4:
      x += fibseq[i];
      y -= fibseq[i];

      vertex(x, y, 0);
      direction = 1;
      break;
    }
    println(x, y);
    xCords[i]= x-512;
    yCords[i] =y-512;
  }
  endShape();

  for (int i = 0; i< 13; i++) {
    fill(unhex(gradient[i]));

    switch(direction) {
    case 1:
      translate( 0.5*fibseq[i], 0.5*fibseq[i], 0);
      box(fibseq[i]);
      translate( 0.5*fibseq[i], 0.5*fibseq[i], 0);
      direction++;
      break;
    case 2:
      translate( -0.5*fibseq[i], 0.5*fibseq[i], 0);
      box(fibseq[i]);
      translate( -0.5*fibseq[i], 0.5*fibseq[i], 0);
      direction++;
      break;
    case 3:
      translate( -0.5*fibseq[i], -0.5*fibseq[i], 0);
      box(fibseq[i]);
      translate( -0.5*fibseq[i], -0.5*fibseq[i], 0);
      direction++;
      break;
    case 4:
      translate( 0.5*fibseq[i], -0.5*fibseq[i], 0);
      box(fibseq[i]);
      translate( 0.5*fibseq[i], -0.5*fibseq[i], 0);
      direction = 1;
      break;
    }
  }
  println("X coordinates");
  println(xCords);
  println("Y coordinates");
  println(yCords);
}
